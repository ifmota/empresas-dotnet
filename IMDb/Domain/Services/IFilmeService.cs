using System.Collections.Generic;
using System.Threading.Tasks;
using IMDb.Domain.Models;

namespace IMDb.Domain.Services
{
    public interface IFilmeService
    {
         IEnumerable<Filme> ListarFilmes(string filtro);
         Task Criar(Filme filme);
         void Votar(int idFilme, int idUsuario, double nota);
         Filme BuscarPorId(int id);
    }
}