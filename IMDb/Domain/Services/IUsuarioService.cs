using System.Collections.Generic;
using System.Threading.Tasks;
using IMDb.Domain.Models;

namespace IMDb.Domain.Services
{
    public interface IUsuarioService
    {
         IEnumerable<Usuario> ListarUsuariosComuns();
         Task Criar(Usuario usuario);
         Task Deletar(int id);
         void Editar(Usuario usuario);
         bool IsAdm(int id);
         Usuario BuscarPorId(int id);
    }
}