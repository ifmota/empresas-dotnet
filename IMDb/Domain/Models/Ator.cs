using System.Collections.Generic;

namespace IMDb.Domain.Models
{
    public class Ator
    {
        public int Id {get; set;}
        public string Nome {get; set;}
        public int FilmeId {get;set;}
        public IList<Filme> Filmes {get;set;} = new List<Filme>();
    }
}