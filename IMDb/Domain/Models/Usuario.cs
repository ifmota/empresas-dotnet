using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IMDb.Domain.Models
{
    public class Usuario
    {
        [Required]
        public int Id {get;set;}
        
        [Required]
        public string Nome {get;set;}
        
        [Required]
        public string Login {get; set;}
        
        [Required]
        public string Senha {get; set;}

        [DefaultValue("cmn")]
        public string Tipo {get; set;}
        
        [DefaultValue("true")]
        public bool Ativo {get; set;}

        public bool IsAdm(){
            if(!String.IsNullOrEmpty(this.Tipo) && this.Tipo.Equals("adm")){
                return true;
            }else{
                return false;
            };
        }
    }
}