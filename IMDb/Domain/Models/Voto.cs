using System.ComponentModel.DataAnnotations;

namespace IMDb.Domain.Models
{
    public class Voto
    {
        public int Id {get; set;}
        [Range(0, 4)]
        public double Nota {get; set;}
        public int UsuarioId {get;set;}
        public int FilmeId {get; set;}
    }
}