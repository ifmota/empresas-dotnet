using System.Collections.Generic;

namespace IMDb.Domain.Models
{
    public class Filme
    {
        public int Id {get; set;}
        public string Nome {get;set;}
        public string Genero {get; set;}
        public double Classificacao {get; set;}
        public string Direção {get; set;}
        public string Atores {get; set;}
    }
}