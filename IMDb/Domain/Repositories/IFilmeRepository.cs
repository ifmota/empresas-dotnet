using System.Collections.Generic;
using System.Threading.Tasks;
using IMDb.Domain.Models;

namespace IMDb.Domain.Repositories
{
    public interface IFilmeRepository
    {
         List<Filme> ListarFilmes(string filtro);
         Task Criar(Filme filme);
         void Editar(Filme filme);
         Filme BuscarPorId(int id);
    }
}