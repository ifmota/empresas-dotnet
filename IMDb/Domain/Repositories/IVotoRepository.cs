using System.Collections.Generic;
using IMDb.Domain.Models;

namespace IMDb.Domain.Repositories
{
    public interface IVotoRepository
    {
         void Votar(int idFilme, int idUsuario, double nota);
         List<Voto> BuscarPorFilmeId(int idFilme);
    }
}