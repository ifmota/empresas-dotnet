using System.Collections.Generic;
using System.Threading.Tasks;
using IMDb.Domain.Models;

namespace IMDb.Domain.Repositories
{
    public interface IUsuarioRepository
    {
         IEnumerable<Usuario> ListarUsuariosComuns();
         Task Criar(Usuario usuario);
         Task Deletar(int id);
         void Editar(Usuario usario);
         Usuario BuscarUsuario(int id);
    }
}