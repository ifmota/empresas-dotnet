using System.Collections.Generic;
using IMDb.Domain.Models;
using IMDb.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace IMDb.Controllers
{
    [Route("/api/[controller]")]
    public class FilmeController : ControllerBase
    {
        private readonly IFilmeService _filmeService;
        private readonly IUsuarioService _usuarioService;

        public FilmeController(IFilmeService filmeService, IUsuarioService usuarioService){
            _filmeService = filmeService;
            _usuarioService = usuarioService;
        }

        [Route("criar")]
        [HttpPost]
        public ActionResult Criar(int idUsuario, Filme filme){
            if(_usuarioService.IsAdm(idUsuario)){
                _filmeService.Criar(filme);
            return Ok();
            }else{
                return BadRequest("Usuário sem permissão.");
            }
        }

        [Route("listar")]
        [HttpGet]
        public ActionResult<IEnumerable<Filme>> Listar(string filtro){
            IEnumerable<Filme> filmesListados =_filmeService.ListarFilmes(filtro);
            return Ok(filmesListados);
        }

        [Route("votar")]
        [HttpPost]
        public ActionResult Votar(int idFilme, int idUsuario, double nota){
            var usuario = _usuarioService.BuscarPorId(idUsuario);
            var filme = _filmeService.BuscarPorId(idFilme);
            if(usuario !=null && filme != null){
                _filmeService.Votar(idFilme, idUsuario, nota);
                return Ok();
            }else{
                return BadRequest();
            }
        }
    }
}