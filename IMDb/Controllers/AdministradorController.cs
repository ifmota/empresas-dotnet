using System;
using System.Collections.Generic;
using System.Net.Http;
using IMDb.Domain.Models;
using IMDb.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace IMDb.Controllers
{
    [Route("/api/[controller]")]
    public class AdministradorController : ControllerBase
    {
        private readonly IUsuarioService _usuarioService;

        public AdministradorController(IUsuarioService usuarioService){
            _usuarioService = usuarioService;
        }

        [Route("lista")]
        [HttpPost]
        public ActionResult<IEnumerable<Usuario>> ListarUsuariosComuns(int idUsuario){
            if(_usuarioService.IsAdm(idUsuario)){
                IEnumerable<Usuario> usuarios = new List<Usuario>();
                usuarios = _usuarioService.ListarUsuariosComuns();
            return Ok(usuarios);
            }else{
                return BadRequest("Usuário sem permissão.");
            }
        }
        
        [Route("criar")]
        [HttpPost]
        public ActionResult Criar(Usuario usuario){
            try{
                usuario.Tipo = "adm";
                _usuarioService.Criar(usuario);
                Console.WriteLine("done");
                return Ok();
            }catch (Exception e){
                return BadRequest(e);
            }
        }

        [Route("deletar/{id:int}")]
        [HttpDelete]
        public ActionResult Deletar(int id){
            try{
                _usuarioService.Deletar(id);
                Console.WriteLine("done");
                return Ok();
            }catch (Exception e){
                return BadRequest(e);
            }
        }

        [Route("editar")]
        [HttpPost]
        public ActionResult Editar(Usuario usuario){
            try{
                _usuarioService.Editar(usuario);
                Console.WriteLine("done");
                return Ok();
            }catch (Exception e){
                return BadRequest(e);
            }
        }
    }
}