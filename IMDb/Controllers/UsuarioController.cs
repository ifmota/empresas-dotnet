using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using IMDb.Domain.Models;
using IMDb.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace IMDb.Controllers
{
    [Route("/api/[controller]")]
    public class UsuarioController
    {
        private readonly IUsuarioService _usuarioService;

        public UsuarioController(IUsuarioService usuarioService){
            _usuarioService = usuarioService;
        }

        [Route("criar")]
        [HttpPost]
        public HttpResponseMessage Criar(Usuario usuario){
            try{
                usuario.Tipo = "cmn";
                _usuarioService.Criar(usuario);
                Console.WriteLine("done");
                return new HttpResponseMessage(HttpStatusCode.OK);
            }catch (Exception e){
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        [Route("deletar/{id:int}")]
        [HttpDelete]
        public HttpResponseMessage Deletar(int id){
            try{
                _usuarioService.Deletar(id);
                Console.WriteLine("done");
                return new HttpResponseMessage(HttpStatusCode.OK);
            }catch (Exception e){
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        [Route("editar")]
        [HttpPost]
        public HttpResponseMessage Editar(Usuario usuario){
            try{
                _usuarioService.Editar(usuario);
                Console.WriteLine("done");
                return new HttpResponseMessage(HttpStatusCode.OK);
            }catch (Exception e){
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}