using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDb.Domain.Models;
using IMDb.Domain.Repositories;
using IMDb.Domain.Services;

namespace IMDb.Services
{
    public class FilmeService : IFilmeService
    {
        private readonly IFilmeRepository _filmeRepository;
        private readonly IVotoRepository _votoRepository;
        public FilmeService(IFilmeRepository filmeRepository, IVotoRepository votoRepository){
            _filmeRepository = filmeRepository;
            _votoRepository = votoRepository;
        }
        public async Task Criar(Filme filme)
        {
            await _filmeRepository.Criar(filme);
        }

        public IEnumerable<Filme> ListarFilmes(string filtro)
        {
            return _filmeRepository.ListarFilmes(filtro);
        }

        public void Votar(int idFilme, int idUsuario, double nota)
        {
            _votoRepository.Votar(idFilme, idUsuario, nota);
            this.AtualizarClassificacao(idFilme);
        }

        public void AtualizarClassificacao(int idFilme){
            List<Voto> votos = _votoRepository.BuscarPorFilmeId(idFilme);
            int qtVoto = votos.Count;
            double somaVoto = votos.Select(p => p.Nota).Sum();
            double media = somaVoto/qtVoto;
            
            Filme filme = _filmeRepository.BuscarPorId(idFilme);
            filme.Classificacao = media;
            _filmeRepository.Editar(filme);
        }

        public Filme BuscarPorId(int id){
            return _filmeRepository.BuscarPorId(id);
        }
    }
}