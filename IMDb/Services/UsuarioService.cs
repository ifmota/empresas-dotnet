using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IMDb.Domain.Models;
using IMDb.Domain.Repositories;
using IMDb.Domain.Services;

namespace IMDb.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usarioRepository;
        public UsuarioService(IUsuarioRepository usuarioRepository){
            _usarioRepository = usuarioRepository;
        }
        public IEnumerable<Usuario> ListarUsuariosComuns(){
            return _usarioRepository.ListarUsuariosComuns();
        }

        public async Task Criar(Usuario usuario){
            await _usarioRepository.Criar(usuario);
        }

        public async Task Deletar(int id){
            await _usarioRepository.Deletar(id);
        }

        public void Editar(Usuario usuario){
            _usarioRepository.Editar(usuario);
        }

        public bool IsAdm(int id){
            Usuario usuario = _usarioRepository.BuscarUsuario(id);
            if(usuario != null){
                return usuario.IsAdm();
            }else{
                return false;
            }
        } 

        public Usuario BuscarPorId(int id){
            return _usarioRepository.BuscarUsuario(id);
        }
    }
}