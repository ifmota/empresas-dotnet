using IMDb.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace IMDb.Persistence.Contexts
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options){}

        public DbSet<Usuario> Usuarios {get;set;}
        public DbSet<Ator> Atores {get;set;}
        public DbSet<Diretor> Diretores {get;set;}
        public DbSet<Filme> Filmes {get;set;}
        public DbSet<Voto> Votos {get;set;}

        protected override void OnModelCreating(ModelBuilder builder){
            base.OnModelCreating(builder);

            builder.Entity<Usuario>().ToTable("Usuarios");
            builder.Entity<Usuario>().HasKey(p => p.Id);
            builder.Entity<Usuario>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();

            builder.Entity<Usuario>().HasData(
                new Usuario {Id = 100, Nome = "Administrador", Login = "adm", Senha = "adm", Tipo = "adm", Ativo = true},
                new Usuario {Id = 101, Nome = "Usuario Comum 1", Login = "usum", Senha = "usum", Tipo = "cmn", Ativo = true}
            );

            builder.Entity<Ator>().ToTable("Atores");
            builder.Entity<Ator>().HasKey(p => p.Id);
            builder.Entity<Ator>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();

            builder.Entity<Diretor>().ToTable("Diretores");
            builder.Entity<Diretor>().HasKey(p => p.Id);
            builder.Entity<Diretor>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();

            builder.Entity<Filme>().ToTable("Filmes");
            builder.Entity<Filme>().HasKey(p => p.Id);
            builder.Entity<Filme>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            
            builder.Entity<Voto>().ToTable("Filmes");
            builder.Entity<Voto>().HasKey(p => p.Id);
            builder.Entity<Voto>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            
        }
    }
}