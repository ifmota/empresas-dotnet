using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDb.Domain.Models;
using IMDb.Domain.Repositories;
using IMDb.Persistence.Contexts;

namespace IMDb.Persistence.Repositories
{
    public class FilmeRepository : BaseRepository, IFilmeRepository
    {
        public FilmeRepository(AppDbContext context) : base(context)
        {}

        public async Task Criar(Filme filme)
        {
            await _context.Filmes.AddAsync(filme);
            _context.SaveChanges();
        }

        public List<Filme> ListarFilmes(string filtro)
        {
            List<Filme> lista = _context.Filmes.ToList();
            var filtrado = string.IsNullOrEmpty(filtro) ? lista : lista 
                .Where(n => n.Nome == filtro)
                .Union(
                    lista.Where(g => g.Genero == filtro)
                )
                .Union(
                    lista.Where(d => d.Direção.Contains(filtro))
                )
                .Union(
                    lista.Where(a => a.Atores.Contains(filtro))
                )
                .OrderBy(p => p.Nome).ThenBy(p => p.Classificacao)
                .ToList();
            
            return filtrado;
        }

        public void Editar(Filme filme){
            _context.Filmes.Update(filme);
            _context.SaveChanges();
        }

        public Filme BuscarPorId(int id){
            return _context.Filmes.FirstOrDefault(p => p.Id == id);
        }
    }
}