using System.Collections.Generic;
using System.Linq;
using IMDb.Domain.Models;
using IMDb.Domain.Repositories;
using IMDb.Persistence.Contexts;

namespace IMDb.Persistence.Repositories
{
    public class VotoRepository: BaseRepository, IVotoRepository
    {
        public VotoRepository(AppDbContext context) : base(context)
        {} 
    

        public void Votar(int idFilme, int idUsuario, double nota)
        {
            Voto voto = _context.Votos.Where(
                p => (p.UsuarioId == idUsuario) &&
                (p.FilmeId == idFilme)
            ).First();

            voto.Nota = nota;

            _context.Votos.Update(voto);
            _context.SaveChanges();
        }

        public List<Voto> BuscarPorFilmeId(int idFilme){
            var votos = _context.Votos.Where(p => p.FilmeId == idFilme).ToList();
            return votos;
        }
    }
}