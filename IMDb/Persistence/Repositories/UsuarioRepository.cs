using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDb.Domain.Models;
using IMDb.Domain.Repositories;
using IMDb.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace IMDb.Persistence.Repositories
{
    public class UsuarioRepository : BaseRepository, IUsuarioRepository
    {
        public UsuarioRepository(AppDbContext context) : base(context)
        {}

        public IEnumerable<Usuario> ListarUsuariosComuns(){
            return _context.Usuarios.Where
                (p => (p.Tipo != "adm") && (p.Ativo == true))
                .OrderBy(p => p.Nome)
                .ToList();
        }

        public async Task Criar(Usuario usuario){
            usuario.Ativo = true;
            await _context.Usuarios.AddAsync(usuario);
            _context.SaveChanges();
        }

        public async Task Deletar(int id){
           Usuario usuario = await _context.Usuarios.FindAsync(id);
           usuario.Ativo = false;
           _context.Usuarios.Update(usuario);
           _context.SaveChanges();
        }

        public void Editar(Usuario usuario){
            _context.Usuarios.Update(usuario);
            _context.SaveChanges();
        }

        public Usuario BuscarUsuario(int id){
           Usuario usuario = _context.Usuarios.Find(id);
           return usuario;
        }
    }
}